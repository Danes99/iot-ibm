# IoT IBM

This repository contains several exercises to understand duality of the Cloud and the internat of things.

## Table of contents

1. [Installation](#Installation)
1. [Usage](#Usage)
1. [Contributing](#Contributing)
1. [Authors and acknowledgment](#Authors-and-acknowledgment)
1. [License](#License)

## Installation

The exercises are meant to be used inside Docker containers.
First download [Docker Desktop](https://www.docker.com/products/docker-desktop) on your machine. Docker is available for Windows, Mac and Linux.
Once you have downloaded Docker desktop, make sure you have the 'docker' and 'docker-compose' CLI installed.
In the new Docker Desktop versions, the 'docker compose' (without '-') CLI pre-installed and can be used for all the 'docker-compose' commands.

## Usage

### Python

```bash
cd python
docker-compose up
```

Or with the newer Docker CLI (I advise to use this one if you can):

```bash
docker compose up
```

### Node-RED

```bash
cd node_red
docker-compose up
```

Or with the newer Docker CLI (I advise to use this one if you can):

```bash
docker compose up
```

Connect with your web browser at <http://localhost:1880/>

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Authors and acknowledgment

Made by Clément Stauner, 2021.

## License

[© MIT](https://mit-license.org/)
