# IoT: Node-RED + MQTT Server & Watson IoT

This exercise allows you to send simulated data (x,y,z) from a simple Python program to a device created with the IBM Watson Internet of Things platform under your IBM Cloud account.

## Table of contents

1. [Installation](#Installation)
1. [Usage](#Usage)
1. [Contributing](#Contributing)
1. [Authors and acknowledgment](#Authors-and-acknowledgment)
1. [License](#License)

## Installation

Download [MQTT.fx](http://www.mqttfx.jensd.de/index.php/download), a software to test publication and subscription on o MQTT server.

### Docker

The MQTT server and the Node-RED web server are meant to be used inside Docker containers.
First download [Docker Desktop](https://www.docker.com/products/docker-desktop) on your machine. Docker is available for Windows, Mac and Linux.
Once you have downloaded Docker desktop, make sure you have the 'docker' and 'docker-compose' CLI installed.
In the new Docker Desktop versions, the 'docker compose' (without '-') CLI is pre-installed and can be used for all the 'docker-compose' commands.

## Usage

### Node-RED

Try using Node-RED to publish and subscribe to the MQTT server.

Remember: If you try to connect to the MQTT server by using 'localhost', it will raise an error because the 'localhost' refers to inside the container.
But in our case me want Node-RED to connect to a MQTT server in another container inside the same Docker network.
Then the IP address will be the container name, here 'broker'.

<img
    src="../docs/img/config_nodered_mqtt.png"
    alt="PgAdmin4 Login"
/>

### MQTT.fx

Try to publish and subscribe to the MQTT server.

```json
{
    "id": 777,
    "data": "some data",
    "metadata": "some metadata",
    "timestamp": "some timestamp"
}
```

### Watson IoT

Try to connect and publish some event on the IBM Cloud using the Watson IoT platform.

<img
    src="../docs/img/config_nodered_wiot.png"
    alt="Watson IoT"
/>

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Authors and acknowledgment

Made by Clément Stauner, 2021.

## License

[© MIT](https://mit-license.org/)
